use tokio::net::TcpListener;
use crate::server::worker::WorkerFactory;

mod tcpsrv;
pub mod worker;

pub async fn start(addr: &str, worker_factory: WorkerFactory) {
    let listener = TcpListener::bind(addr).
        await.
        expect(format!("couldn't bind to addr {:?}", addr).as_str());

    let mut tcp_srv = tcpsrv::TcpServer { worker_factory, listener };

    match tcp_srv.serve().await {
        Ok(..) => {
            println!("tcp server has stopped")
        }
        Err(e) => {
            println!("tcp server has stopped with error: {}", e)
        }
    }
}
