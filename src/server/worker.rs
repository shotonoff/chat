use std::sync::Arc;
use anyhow::{anyhow, Error};
use diesel::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};
use tokio::{join, select};
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio::sync::mpsc::{Receiver, Sender};
use crate::models::Message;
use crate::net::conn::Conn;
use crate::net::message::{HandshakeMessage, MessageKind, SubscribeMessage, TextMessage, UnsubscribeMessage};
use crate::pubsub::{PubSub, Subscription};
use crate::repo::{Repository};

pub(crate) trait WorkerRunner {
    async fn run(&mut self) -> Result<(), Error>;
}

pub enum Worker {
    Message(MessageWorker),
    Conn(ConnWorker),
}

pub struct MessageWorker {
    pub tx: Sender<MessageKind>,
    pub rx: Receiver<MessageKind>,
    pub pubsub: Arc<PubSub>,
    pub msg_repo: Arc<Repository>,
}

impl MessageWorker {
    async fn handshake_handler(&self, _msg: HandshakeMessage) -> Result<(), Error> {
        self.send_ack_ok().await
    }

    async fn text_handler(&self, msg: TextMessage) -> Result<(), Error> {
        let cloned_msg = msg.clone();
        let new_msg = Message::new_from_text_message(&msg);
        self.msg_repo
            .create(new_msg)
            .expect("Failed to create a message");
        let (r1, r2) = join!(
            self.send_ack_ok(),
            self.broadcast(MessageKind::Text(cloned_msg)),
        );
        r1.map_err(|e| anyhow!("failed to send a message: {}", e))?;
        r2.map_err(|e| anyhow!("failed to broadcast a message: {}", e))
    }

    async fn subscribe_handler(&self, msg: SubscribeMessage) -> Result<(), Error> {
        let sub = Subscription {
            topic_name: msg.topic_name,
            client_id: msg.id,
            msg_tx: self.tx.clone(),
        };
        self.pubsub.
            subscribe(sub).
            await.
            map_err(|e| anyhow!("failed to subscribe: {}", e))?;
        self.send_ack_ok().await
    }

    async fn unsubscribe_handler(&self, _msg: UnsubscribeMessage) -> Result<(), Error> {
        self.send_ack_ok().await
    }

    async fn send_ack_ok(&self) -> Result<(), Error> {
        self.tx.
            send(MessageKind::Ok("ok".to_string())).
            await.
            map_err(|e| anyhow!("failed to ack: {}", e))
    }

    async fn broadcast(&self, msg: MessageKind) -> Result<(), Error> {
        self.pubsub.publish(msg).await
    }
}

impl WorkerRunner for MessageWorker {
    async fn run(&mut self) -> Result<(), Error> {
        loop {
            let msg = self.rx.recv().await;
            if msg.is_none() {
                println!("MessageWorker: received is none message");
                return Ok(());
            }
            match msg.unwrap() {
                MessageKind::Handshake(msg) => self.handshake_handler(msg).await?,
                MessageKind::Text(msg) => self.text_handler(msg).await?,
                MessageKind::Subscribe(msg) => self.subscribe_handler(msg).await?,
                MessageKind::Unsubscribe(msg) => self.unsubscribe_handler(msg).await?,
                MessageKind::Ok(_text) => ()
            }
        }
    }
}


impl WorkerRunner for Worker {
    async fn run(&mut self) -> Result<(), Error> {
        match self {
            Worker::Message(ref mut w) => w.run().await,
            Worker::Conn(ref mut w) => w.run().await,
        }
    }
}

pub struct ConnWorker {
    pub conn: Arc<Conn>,
    pub tx: Sender<MessageKind>,
    pub rx: Receiver<MessageKind>,
}

impl WorkerRunner for ConnWorker {
    async fn run(&mut self) -> Result<(), Error> {
        loop {
            select! {
                    msg = self.rx.recv() => match msg {
                        None => {
                            println!("ConnWorker: received none message");
                            return Ok(())
                        }
                        Some(msg) => {
                            println!("received message: {:?}", msg);
                            if let Err(err) = self.conn.send(msg).await {
                                println!("message sending failed: {}", err);
                            };
                        }
                    },
                    msg = self.conn.recv() => match msg {
                        Ok(msg) => {
                            println!("received message: {:?}", msg);
                            if let Err(err) = self.tx.send(msg).await {
                                println!("message sending failed: {}", err);
                            };
                        }
                        Err(err) => {
                            println!("conn recv error:  {}", err);
                            return Err(err)
                        },
                    }
                }
        }
    }
}

pub struct WorkerFactory {
    pub pool: Pool<ConnectionManager<PgConnection>>,
    pub pub_sub: Arc<PubSub>,
}

impl WorkerFactory {
    pub fn new(pub_sub: PubSub, pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pub_sub: Arc::new(pub_sub), pool }
    }

    fn new_message_worker(&self, tx: Sender<MessageKind>, rx: Receiver<MessageKind>) -> MessageWorker {
        let cloned_pool = self.pool.clone();
        MessageWorker {
            tx,
            rx,
            pubsub: self.pub_sub.clone(),
            msg_repo: Arc::new(Repository::new(cloned_pool)),
        }
    }

    fn new_conn_worker(&self, stream: TcpStream, tx: Sender<MessageKind>, rx: Receiver<MessageKind>) -> ConnWorker {
        ConnWorker { tx, rx, conn: Arc::new(Conn::new(stream)) }
    }

    pub fn new_workers(&self, stream: TcpStream) -> Vec<Worker> {
        let (send_tx, send_rx) = mpsc::channel::<MessageKind>(32);
        let (recv_tx, recv_rx) = mpsc::channel::<MessageKind>(32);

        vec![
            Worker::Message(self.new_message_worker(send_tx, recv_rx)),
            Worker::Conn(self.new_conn_worker(stream, recv_tx, send_rx)),
        ]
    }
}