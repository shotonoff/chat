use anyhow::{anyhow};
use tokio::net::{TcpListener};
use crate::server::worker::{WorkerFactory, WorkerRunner};

pub(crate) struct TcpServer {
    pub listener: TcpListener,
    pub worker_factory: WorkerFactory,
}

impl TcpServer {
    // fn connected(&self, stream: TcpStream) -> Vec<Worker> {
    //     let (send_tx, send_rx) = mpsc::channel::<MessageKind>(32);
    //     let (recv_tx, recv_rx) = mpsc::channel::<MessageKind>(32);
    //
    //     let conn = Arc::new(Conn::new(stream));
    //
    //     let conn_worker = ConnWorker {
    //         tx: recv_tx,
    //         rx: send_rx,
    //         conn,
    //     };
    //
    //     let msg_worker = MessageWorker {
    //         tx: send_tx,
    //         rx: recv_rx,
    //         pubsub: self.pubsub.clone(),
    //         // msg_repo: ,
    //     };
    //
    //     vec![
    //         Worker::Message(msg_worker),
    //         Worker::Conn(conn_worker),
    //     ]
    // }

    pub async fn serve(&mut self) -> Result<(), anyhow::Error> {
        let listener = &self.listener;
        loop {
            let (stream, _) = listener.
                accept().
                await.
                map_err(|e| anyhow!("couldn't accept stream: {}", e))?;

            let workers = self.worker_factory.new_workers(stream);
            for mut worker in workers {
                tokio::spawn(async move {
                    if let Err(e) = worker.run().await {
                        println!("worked has been stopped with the error: {}", e)
                    }
                });
            }
        }
    }
}
