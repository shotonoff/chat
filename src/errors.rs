use std::io;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Got io error: {0}")]
    IO(io::Error),
    #[error("Got io error: {0}")]
    DB(String),
}
