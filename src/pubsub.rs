use std::collections::HashMap;
use std::sync::{Arc};
use anyhow::anyhow;
use tokio::sync::mpsc::Sender;
use tokio::sync::Mutex;
use crate::net::message::{MessageKind};

#[derive(Clone)]
pub struct PubSub {
    topic_subs: Arc<Mutex<HashMap<String, Vec<Subscription>>>>,
}

pub struct Subscription {
    pub topic_name: String,
    pub client_id: u64,
    pub msg_tx: Sender<MessageKind>,
}

impl PubSub {
    pub fn new() -> Self {
        Self {
            topic_subs: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn subscribe(&self, sub: Subscription) -> Result<(), anyhow::Error> {
        let mut topic_subs = self.topic_subs.lock().await;
        let subs = topic_subs.get_mut(sub.topic_name.as_str());
        match subs {
            Some(subs) => {
                subs.push(sub);
            }
            None => {
                let topic_name = sub.topic_name.to_owned();
                topic_subs.insert(topic_name, vec![sub]);
            }
        };
        Ok(())
    }

    pub async fn unsubscribe(&mut self, channel: &str, client_id: u64) -> Result<(), ()> {
        let mut chans = self.topic_subs.lock().await;
        let subs = chans.get_mut(channel);

        if subs.is_none() {
            return Ok(());
        }

        let subs = subs.unwrap();

        let pos = subs.iter().position(|x| x.client_id == client_id);

        if pos.is_none() {
            return Ok(());
        }

        subs.remove(pos.unwrap());

        Ok(())
    }

    pub async fn publish(&self, msg: MessageKind) -> Result<(), anyhow::Error> {
        match msg.clone() {
            MessageKind::Text(msg) => self.broadcast(
                msg.topic_name.to_owned().as_str(),
                msg.sender_id,
                MessageKind::Text(msg),
            ).await,
            _ => Err(anyhow!("given a message is not possible to broadcast"))
        }
    }

    async fn broadcast(&self, topic_name: &str, sender_id: u64, msg: MessageKind) -> Result<(), anyhow::Error> {
        let subs = self.topic_subs.lock().await;
        let subs = subs.get(topic_name);
        if subs.is_none() {
            return Ok(());
        }
        for sub in subs.unwrap() {
            let msg_cloned = msg.clone();
            if sub.client_id == sender_id {
                continue;
            }
            let res = sub.msg_tx.send(msg_cloned).await;
            if let Err(err) = res {
                println!("failed to publish message: {}", err)
            }
        }
        Ok(())
    }
}
