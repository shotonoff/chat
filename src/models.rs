use std::fmt::{Debug};
use chrono::{DateTime, Utc};
use diesel::{AsExpression, FromSqlRow, Insertable, Queryable, Selectable};
use diesel::backend::Backend;
use diesel::deserialize::FromSql;
use diesel::pg::Pg;
use diesel::serialize::{IsNull, Output, ToSql};
use crate::net::message::TextMessage;
use crate::snowflake;
use diesel::sql_types::SmallInt;

#[derive(Debug, Queryable, Insertable, Selectable)]
#[diesel(table_name = crate::schema::messages)]
pub struct Message {
    pub id: String,
    pub data: String,
    pub format: MessageFormat,
    pub topic: String,
    pub sender_id: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

impl Message {
    pub fn new_from_text_message(msg: &TextMessage) -> Self {
        use base64::{engine::general_purpose, Engine as _};
        return Self {
            id: snowflake::encode(msg.id),
            data: msg.text.clone(),
            format: MessageFormat::Text,
            topic: msg.topic_name.to_owned(),
            sender_id: general_purpose::STANDARD.encode(msg.sender_id.to_be_bytes()),
            created_at: chrono::offset::Utc::now(),
            updated_at: chrono::offset::Utc::now(),
        };
    }
}

#[derive(Default, Debug, Clone, Copy, AsExpression, FromSqlRow)]
#[diesel(sql_type = SmallInt)]
pub enum MessageFormat {
    #[default]
    Text = 0
}

impl ToSql<SmallInt, Pg> for MessageFormat {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> diesel::serialize::Result {
        match *self {
            MessageFormat::Text => <i16 as ToSql<SmallInt, Pg>>::to_sql(&0i16, out)?,
        };
        Ok(IsNull::No)
    }
}

impl FromSql<SmallInt, Pg> for MessageFormat {
    fn from_sql(bytes: <Pg as Backend>::RawValue<'_>) -> diesel::deserialize::Result<Self> {
        match i16::from_sql(bytes)? {
            0 => Ok(MessageFormat::Text),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}
