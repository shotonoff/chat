use anyhow::anyhow;
use tokio::sync::{mpsc, oneshot};
use crate::client::command::Command;
use crate::net::message::MessageKind;

pub struct Client {
    tx: mpsc::Sender<Command>,
}

impl Client {
    pub fn new(tx: mpsc::Sender<Command>) -> Self {
        Self { tx }
    }

    pub async fn send(&self, msg: MessageKind) -> Result<(), anyhow::Error> {
        let (tx, rx) = oneshot::channel();
        let cmd = Command { msg, result_ch: tx };
        self.tx.
            send(cmd).
            await.
            map_err(|e| anyhow!("failed to send a command: {}", e))?;
        rx.
            await.
            map_err(|e| anyhow!("failed to receive result: {}", e))?;
        Ok(())
    }
}
