use std::sync::Arc;
use anyhow::anyhow;
use tokio::sync::oneshot;
use crate::consumer::ConsumerHandler;
use crate::net::conn::Conn;
use crate::net::message::MessageKind;
use crate::net::package::Package;

#[derive(Debug)]
pub struct Command {
    pub msg: MessageKind,
    pub result_ch: oneshot::Sender<Option<()>>,
}

impl Command {
    pub async fn notify(self, result: Option<()>) {
        self.result_ch.send(result).expect("failed to send result");
    }
}

pub struct CommandHandler {
    pub conn: Arc<Conn>,
}

impl ConsumerHandler<Command> for CommandHandler {
    type Error = anyhow::Error;
    type Output = ();

    async fn handle(&self, cmd: Command) -> Result<Self::Output, Self::Error> {
        let pkg: Package = cmd.msg.
            clone().
            try_into().
            map_err(|e|anyhow!("failed to convert a command to a package: {}", e))?;

        self.conn.send(pkg).await?;

        let msg: MessageKind = self.
            conn.
            recv().
            await.
            map_err(|e|anyhow!("failed to receive response: {}", e))?.
            try_into().
            map_err(|e|anyhow!("failed to convert package to message: {}", e))?;

        if let MessageKind::Text(msg) = msg {
            println!("> [{}] {}: {}", msg.topic_name, msg.sender_id, msg.text)
        }

        Ok(cmd.notify(Some(())).await)
    }
}
