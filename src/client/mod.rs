pub mod client;
mod command;

use std::sync::Arc;
use std::sync::atomic::{AtomicU64, Ordering};
use std::time::Duration;
use anyhow::anyhow;
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio::time::sleep;
use crate::client::client::Client;
use crate::client::command::{Command, CommandHandler};
use crate::consumer::Consumer;
use crate::net::conn::Conn;
use crate::net::message::{HandshakeMessage, MessageKind, SubscribeMessage, TextMessage};
use crate::snowflake::{Generator};
use crate::user::User;

pub async fn start(addr: &str, user: User) -> Result<(), anyhow::Error> {
    let public_topic_name = "public";

    let stream = TcpStream::connect(addr).
        await.
        map_err(|e| anyhow!("server is unavailable: {}", e))?;

    let conn = Arc::new(Conn::new(stream));
    let (tx, rx) = mpsc::channel::<Command>(32);
    let client = Client::new(tx);
    let handler = CommandHandler { conn };
    let mut consumer = Consumer::new(rx, handler);

    tokio::spawn(async move {
        if let Err(e) = consumer.consume().await {
            println!("a consumer got error: {}", e)
        }
    });

    client.send(MessageKind::Handshake(HandshakeMessage {
        name: user.name.to_owned(),
    })).await?;
    client.send(MessageKind::Subscribe(SubscribeMessage {
        id: user.id,
        topic_name: public_topic_name.to_string(),
    })).await?;

    let counter = AtomicU64::new(0);

    let gen = Generator::new();
    loop {
        let numb = counter.fetch_add(1, Ordering::Release);
        let msg = TextMessage {
            id: gen.generate().into(),
            text: format!("text #{}", numb),
            topic_name: public_topic_name.to_string(),
            sender_id: user.id,
        };
        client.send(MessageKind::Text(msg)).await?;
        sleep(Duration::from_secs(5)).await;
    }
}
