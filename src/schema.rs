// @generated automatically by Diesel CLI.

diesel::table! {
    messages (id) {
        #[max_length = 255]
        id -> Varchar,
        data -> Text,
        format -> Int2,
        topic -> Text,
        #[max_length = 255]
        sender_id -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    posts (id) {
        id -> Int4,
        title -> Varchar,
        body -> Text,
        published -> Bool,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    messages,
    posts,
);
