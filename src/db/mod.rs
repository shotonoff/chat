use diesel::r2d2::{self, ConnectionManager};
use diesel::PgConnection;
use r2d2::PooledConnection;

pub type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;
pub type PooledConn = PooledConnection<ConnectionManager<PgConnection>>;
