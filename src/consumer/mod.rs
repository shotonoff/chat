use std::future::Future;
use anyhow::anyhow;
use tokio::select;
use tokio::sync::mpsc::Receiver;

pub trait ConsumerHandler<T> {
    type Error;
    type Output;
    fn handle(&self, cmd: T) -> impl Future<Output=Result<Self::Output, Self::Error>>;
}

pub struct Consumer<T, H>
    where H: ConsumerHandler<T, Error=anyhow::Error, Output=()> {
    rx: Receiver<T>,
    handler: H,
}

impl<T, H> Consumer<T, H>
    where H: ConsumerHandler<T, Error=anyhow::Error, Output=()> {
    pub fn new(
        rx: Receiver<T>,
        handler: H,
    ) -> Self {
        Self { rx, handler }
    }

    pub async fn consume(&mut self) -> Result<(), anyhow::Error> {
        loop {
            select! {
                cmd = self.rx.recv() => match cmd {
                    None => {
                        println!("got none, stop consuming");
                        return Ok(());
                    }
                    Some(cmd) => {
                        self.handler.
                            handle(cmd).
                            await.
                            map_err(|e|anyhow!("got error: {}", e))?
                    }
                }
            }
        }
    }
}
