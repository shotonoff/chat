use serde::{Serialize, Deserialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct User {
    pub id: u64,
    pub name: String,
}

pub struct UserRepository {
    users: Vec<User>,
}

impl UserRepository {
    pub fn new() -> Self {
        Self {
            users: vec![
                User {
                    id: 100,
                    name: "user 1".to_string(),
                },
                User {
                    id: 200,
                    name: "user 2".to_string(),
                },
                User {
                    id: 300,
                    name: "user 3".to_string(),
                },
            ],
        }
    }

    pub fn get(&self, id: u64) -> Option<User> {
        let pos = self.
            users.
            iter().
            position(|user| user.id == id).
            unwrap();
        self.users.
            get(pos).
            map(|u| u.clone())
    }
}
