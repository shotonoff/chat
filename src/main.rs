use std::env;
use chat::pubsub::PubSub;
use chat::{establish_connection, server};
use chat::server::worker::WorkerFactory;

#[tokio::main]
async fn main() {
    let addr = env::var("ADDR").
        unwrap_or_else(|_| "127.0.0.1:5344".to_string());

    // let (client, connection) =
    //     tokio_postgres::connect("host=localhost user=postgres", NoTls).await?;

    let db_user = "postgres";
    let db_password = "secret";
    let db_host = "localhost:5432";
    let db_name = "chat";

    // echo DATABASE_URL=postgres://username:password@localhost/diesel_demo > .env
    // echo DATABASE_URL=postgres://postgres:secret@db/chat > .env

    let uri = format!("postgres://{0}:{1}@{2}/{3}", db_user, db_password, db_host, db_name);

    let pool = establish_connection(uri.as_str());

    // tokio::spawn(async move {
    //     if let Err(e) = connection.await {
    //         eprintln!("connection error: {}", e);
    //     }
    // });
    let worker_factory = WorkerFactory::new(PubSub::new(), pool);

    println!("app is running: addr={:?}", addr);

    server::start(addr.as_str(), worker_factory).await
}
