use diesel::{PgConnection};
use diesel::r2d2::ConnectionManager;
use crate::db::DbPool;

pub mod pubsub;
pub mod net;
pub mod server;
pub mod client;
pub mod user;
pub mod errors;
pub mod consumer;
pub mod repo;
pub mod snowflake;
pub mod schema;
pub mod db;
pub mod models;

pub fn establish_connection(url: &str) -> DbPool {
    let manager = ConnectionManager::<PgConnection>::new(url);
    r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.")
}
