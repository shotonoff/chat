use std::env;
use chat::client;
use chat::user::UserRepository;

#[tokio::main]
async fn main() {
    let addr = env::var("ADDR").
        unwrap_or_else(|_| "127.0.0.1:5344".to_string());

    let user_repo = UserRepository::new();

    let user_id = env::var("ID").
        expect("id parameter is mandatory").
        parse::<u64>().
        expect("id must be integer");
    let user = user_repo.
        get(user_id).
        expect(format!("user by {:?} not found", user_id).as_str());
    if let Err(e) = client::start(addr.as_str(), user).await {
        println!("client has stopped with an error: {}", e)
    }
}
