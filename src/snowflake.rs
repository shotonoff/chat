use std::collections::HashMap;
use std::fmt::Write;
use std::sync::atomic::{AtomicU8, Ordering};
use chrono::Utc;

const WORKER_ID_BITS: u32 = 4;
const SEQUENCE_BIT: u32 = 8;

// static mut SEQUENCE: AtomicU8 = AtomicU8::new(0);

#[derive(Debug)]
pub struct ID(u64);

pub struct Generator {
    sequence: AtomicU8,
}

const ALPHANUM: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

pub fn encode(mut val: u64) -> String {
    let len = ALPHANUM.len() as u64;
    let chars: Vec<char> = ALPHANUM.chars().collect();
    let mut buf = String::new();
    while val > 0 {
        match chars.get((val % len) as usize) {
            Some(v) => buf.write_char(*v).expect("failed to write a char"),
            None => panic!("out of bound")
        };
        val = val / len;
    }
    format!("{}{}", buf, "0".repeat(16 - buf.len()))
}

pub fn decode(s: &str) -> u64 {
    let (_, m) = ALPHANUM
        .chars()
        .fold((0, HashMap::<char, u64>::new()), |(i, mut m), ch| {
            m.insert(ch, i);
            (i + 1, m)
        });
    let (_, v) = s.chars()
        .fold((0u64, 0u64), |(i, v), ch| {
            let n = m.get(&ch).unwrap();
            (i + 1, v + *n * 16u64.pow(i as u32))
        });
    v
}

impl Into<String> for ID {
    fn into(self) -> String {
        encode(self.0)
    }
}


impl Generator {
    pub fn new() -> Self {
        Self {
            sequence: AtomicU8::new(0),
        }
    }

    pub fn generate(&self) -> ID {
        let worker_id: u64 = 1 << WORKER_ID_BITS;
        let seq = self.sequence.fetch_update(Ordering::Relaxed, Ordering::Relaxed, |v| {
            if v == 255 {
                return Some(0);
            }
            Some(v + 1)
        }).expect("TODO: panic message");

        let ts = Utc::now().timestamp() as u64;
        ID(
            (ts << WORKER_ID_BITS + SEQUENCE_BIT) | ((seq as u32) << SEQUENCE_BIT) as u64 | (worker_id << WORKER_ID_BITS)
        )
    }
}

impl Into<u64> for ID {
    fn into(self) -> u64 {
        self.0
    }
}

impl Into<ID> for u64 {
    fn into(self) -> ID {
        ID(self)
    }
}

#[cfg(test)]
mod test {
    use crate::snowflake::{encode, Generator, ID};

    #[test]
    fn test_snowflake_id_generate() {
        let gen = Generator::new();
        let id = gen.generate();
        println!("{:?}", id)
    }

    #[test]
    fn test_encode() {
        let s = encode(7030695284992);
        assert_eq!("q44UtW9b00000000", s)
    }
}
