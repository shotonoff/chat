use anyhow::Error;
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use crate::net::package::Package;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum MessageKind {
    Handshake(HandshakeMessage),
    Text(TextMessage),
    Subscribe(SubscribeMessage),
    Unsubscribe(UnsubscribeMessage),
    Ok(String),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct HandshakeMessage {
    pub name: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TextMessage {
    pub id: u64,
    pub topic_name: String,
    pub sender_id: u64,
    pub text: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SubscribeMessage {
    pub topic_name: String,
    pub id: u64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UnsubscribeMessage {
    pub channel: String,
}


impl TryInto<Package> for MessageKind {
    type Error = Error;

    fn try_into(self) -> Result<Package, Self::Error> {
        let msg = serde_json::to_vec(&self)?;
        Ok(Package {
            id: Uuid::new_v4(),
            msg_type: self.into(),
            payload: msg,
        })
    }
}

impl TryInto<MessageKind> for &[u8] {
    type Error = Error;

    fn try_into(self) -> Result<MessageKind, Self::Error> {
        let s = String::from_utf8(self.to_vec())?;
        let msg = serde_json::from_str::<MessageKind>(s.as_str())?;
        Ok(msg)
    }
}

impl Into<u8> for MessageKind {
    fn into(self) -> u8 {
        match self {
            MessageKind::Handshake { .. } => 1,
            MessageKind::Text { .. } => 2,
            MessageKind::Subscribe { .. } => 3,
            MessageKind::Unsubscribe { .. } => 4,
            MessageKind::Ok(..) => 5,
        }
    }
}
