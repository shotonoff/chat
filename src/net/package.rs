use std::convert::TryInto;
use anyhow::anyhow;
use uuid::Uuid;
use crate::net::message::MessageKind;

#[derive(Clone, Debug)]
pub struct Package {
    pub id: uuid::Uuid,
    pub msg_type: u8,
    pub payload: Vec<u8>,
}
impl TryInto<Package> for Vec<u8> {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<Package, Self::Error> {
        Package::decode(self.as_slice())
    }
}
impl Package {
    pub fn decode(payload: &[u8]) -> Result<Package, anyhow::Error> {
        Ok(Package {
            msg_type: payload[0].try_into()?,
            id: Uuid::from_slice(&payload[1..17]).
                map_err(|e| anyhow!("failed to convert bytes to uuid: {}", e))?,
            payload: payload[17..].to_owned(),
        })
    }

    pub fn encode(&self) -> Result<Vec<u8>, anyhow::Error> {
        let mut buf = Vec::new();
        buf.push(self.msg_type.into());
        buf.extend_from_slice(self.id.as_bytes());
        buf.extend_from_slice(self.payload.as_slice());
        Ok(buf)
    }
}

impl TryInto<MessageKind> for Package {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<MessageKind, Self::Error> {
        Ok(serde_json::from_slice(self.payload.as_slice())?)
    }
}
