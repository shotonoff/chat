use std::convert::TryInto;
use anyhow::anyhow;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;
use tokio::sync::Mutex;
use crate::errors;
use crate::net::message::MessageKind;
use crate::net::package::Package;

pub struct Conn {
    pub stream: Mutex<TcpStream>,
}

impl Conn {
    pub fn new(stream: TcpStream) -> Self {
        Self {
            stream: Mutex::new(stream),
        }
    }

    pub async fn write(&self, payload: &[u8]) -> Result<usize, errors::Error> {
        let mut stream = self.stream.lock().await;
        stream.write_u32(payload.len() as u32).await.map_err(|e| errors::Error::IO(e))?;
        Ok(stream.write(payload).await.map_err(|e| errors::Error::IO(e))?)
    }

    pub async fn read_package(&self) -> Result<Package, anyhow::Error> {
        let pkg_len = self.read_package_len().await?;
        Ok(self.read_package_data(pkg_len).await?.try_into()?)
    }

    async fn read_package_len(&self) -> Result<u32, errors::Error> {
        let mut stream = self.stream.lock().await;
        let len = stream.read_u32().await.map_err(|e| errors::Error::IO(e))?;
        Ok(len)
    }

    async fn read_package_data(&self, pkg_len: u32) -> Result<Vec<u8>, errors::Error> {
        let mut stream = self.stream.lock().await;

        let mut buf: Vec<u8> = vec![0; pkg_len as usize];

        match stream.read_exact(buf.as_mut_slice()).await {
            Ok(_read_bytes) => Ok(buf),
            Err(e) => Err(errors::Error::IO(e)),
        }
    }

    pub async fn send<T>(&self, msg: T) -> Result<(), anyhow::Error>
        where T: TryInto<Package> {
        let pkg = match msg.try_into() {
            Ok(o) => o,
            Err(..) => return Err(anyhow!("couldn't convert a message to a package")),
        };
        let raw = pkg.encode()?;
        self.write(raw.as_slice()).await?;
        Ok(())
    }

    pub async fn recv(&self) -> Result<MessageKind, anyhow::Error> {
        let pkg = self.read_package().await?;
        Ok(pkg.
            try_into().
            map_err(|e| anyhow!("failed to convert package to message: {}", e))?,
        )
    }
}
