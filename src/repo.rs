use diesel::{insert_into};
use diesel::prelude::*;
use crate::{errors};
use crate::db::{DbPool, PooledConn};
use crate::schema::messages::dsl::*;
use crate::models::Message;

pub struct Repository {
    pool: DbPool,
}

impl Repository {
    pub fn new(pool: DbPool) -> Self {
        Repository { pool }
    }

    pub fn create(&self, msg: Message) -> Result<(), errors::Error> {
        let mut conn = self.get_conn();

        insert_into(messages)
            .values(&msg)
            .execute(&mut conn)
            .map_err(|e| errors::Error::DB(e.to_string()))?;

        Ok(())
    }

    fn get_conn(&self) -> PooledConn {
        self.pool.get().expect("failed to get connection from pool")
    }

    pub fn get_topic_messages(&self, topic_name: &str) -> Result<Option<Vec<Message>>, errors::Error> {
        let mut conn  = self.get_conn();
        let results = messages
            .filter(topic.eq(topic_name))
            .order_by(created_at.desc())
            .limit(10)
            .select(Message::as_select())
            .load(&mut conn);
        match results {
            Ok(res) => Ok(Some(res)),
            Err(_) => Err(errors::Error::DB("failed to fetch data".to_string())),
        }
    }
}
