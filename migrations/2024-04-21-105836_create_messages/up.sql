CREATE TABLE messages
(
    id         VARCHAR(255) PRIMARY KEY,
    data       TEXT                     NOT NULL,
    format     SMALLINT                 NOT NULL,
    topic      TEXT                     NOT NULL,
    sender_id  VARCHAR(255)             NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);
